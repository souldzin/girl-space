﻿using UnityEngine;

public class CelestialWalker : MonoBehaviour {
	private SpriteRenderer _spriteRenderer;

	private Rigidbody2D _rigidBody;

	private bool _isInAir = false;

	public float jumpImpulse = 10;
	public GameObject celestialBody;
	public int speed = 60;
	

	// Use this for initialization
	void Start () {
		_spriteRenderer = GetComponent<SpriteRenderer>();
		_rigidBody = GetComponent<Rigidbody2D>();
	}

	// Update is called once per frame
	void Update () {
		if (Input.GetKey(KeyCode.LeftArrow)) {
			_spriteRenderer.flipX = false;
			Move(Vector3.forward);
		}
		if (Input.GetKey(KeyCode.RightArrow)) {
			_spriteRenderer.flipX = true;
			Move(Vector3.back);
		}
		if (_isInAir == false && Input.GetKey(KeyCode.UpArrow)) {
			_isInAir = true;
			_rigidBody.AddForce(transform.up * jumpImpulse, ForceMode2D.Impulse);
		}
	}

	void Move(Vector3 direction) {
		transform.RotateAround(
			celestialBody.transform.position, 
			direction,
			speed * Time.deltaTime
			);
	}
}
