﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomRepeatingBackground : MonoBehaviour {
	Vector3 direction = Vector3.left;
	float speed = 0.05f;
	float threshold = -12.9f;

	// Use this for initialization
	void Start () {
		Debug.Log("Creating!");
		InitRenderer();
	}
	
	// Update is called once per frame
	void Update () {
		this.transform.position += direction * speed;

		if(this.transform.position.x <= threshold) {
			this.Reincarnate();
		}
	}

	private void InitRenderer() {
		var spriteRenderer = GetComponent<SpriteRenderer>();

		spriteRenderer.flipX = Random.value > 0.5;
		spriteRenderer.flipY = Random.value > 0.5;
		Debug.Log($"{this.gameObject.name} - {spriteRenderer.flipX} {spriteRenderer.flipY}");
	}

	private void Reincarnate() {
		var name = this.gameObject.name;

		var clone = Instantiate(this.gameObject);
		clone.transform.position = new Vector3(-threshold, this.transform.position.y, this.transform.position.z);
		clone.transform.parent = this.transform.parent;
		Destroy(this.gameObject);
		clone.name = name;
	}
}
