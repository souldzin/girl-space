﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mover : MonoBehaviour {
	public float speed = 0.5f;
	public GameObject lookAt;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKey(KeyCode.J)) {
			this.Move(new Vector3(0.0f, 1.0f, 0.0f));
		}		
		if (Input.GetKey(KeyCode.K)) {
			this.Move(new Vector3(0.0f, -1.0f, 0.0f));
		}		
		if (Input.GetKey(KeyCode.H)) {
			this.Move(new Vector3(-1.0f, 0.0f, 0.0f));
		}		
		if (Input.GetKey(KeyCode.L)) {
			this.Move(new Vector3(1.0f, -0.0f, 0.0f));
		}		
	}

	private void Move(Vector3 direction) {
		this.transform.position += direction * speed;
		this.transform.LookAt(lookAt.transform);
	}
}
