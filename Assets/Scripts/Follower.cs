﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Follower : MonoBehaviour {

	private ParticleSystem ps;

	public float speed = 0.25f;
	public GameObject target;

	// Use this for initialization
	void Start () {
		ps = GetComponent<ParticleSystem>();
	}
	
	// Update is called once per frame
	void Update () {
		var shape = ps.shape;

		//find the vector pointing from our position to the target
		var direction = (target.transform.position - this.transform.position).normalized;

		//create the rotation we need to be in to look at the target
		var lookRotation = Quaternion.LookRotation(direction).eulerAngles;

		//rotate us over time according to speed until we are in the required rotation
		shape.rotation = lookRotation;
	}

	private void Step() {
		var diff = this.CalculateDiff();
		var magnitude = diff.magnitude;

		Debug.Log("diff: " + diff.ToString());
		Debug.Log("magnitude: " + magnitude);

		if (magnitude == 0) {
			return;
		}

		var clampedDiff = diff.normalized * Math.Min(speed, magnitude);
		this.transform.position += clampedDiff;
	}

	private Vector3 CalculateDiff() {
		var diff = target.transform.position - this.transform.position;
		diff.z = 0;
		return diff;
	}
}
